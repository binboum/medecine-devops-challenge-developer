# DevOps Challenge - Medecine PubSub

# Introduction

I took up the Loft Orbital DevOps challenge, it was an interesting and very challenging subject that allowed me to learn new things, thank you.

# Prerequisites

- GKE
- GITLAB

# Technological choice

- Python for micro-services development 
- Gitlab to host the repositories GIT, and thus be based on the functionalities as docker images, and also of states terraform.
- Gitlab CI for the Infra as Code part, notably to build the docker images, and deploy the infrastructure on GKE via Terraform.
- Helm to integrate my Kubernetes manifests in a templatized way.
- FluxCD with the operator HelmRelease for the Kubernetes eco-system, and micro-services deployments.
- Redis to be able to manage the interconnection of micro-service.
- Prometheus to retrieve the good metrics when it comes to the scalability of our application.

# Get Started tutorial

Ideally if you want to deploy the project on your own, I advise you to fork the project and then follow the following instructions :

You must first configure the files to configure the future infrastructure with your credentials.

Note that in business systems for the management of passwords, and its automation, we can use safes, such as Vault from Hashicorp which can be more reliable, and which also makes it possible to avoid interactions with sensitive data.

## Gitlab SVC

Kubernetes will need read access to the image registry offered by Gitlab, so you need to generate a Token here `Settings -> Repository -> Deploy Tokens`, you can tick only `read_registry`.

Once created, it is necessary to define 3 variables in your Git repository, one of which is hidden (the password of the service account).

You can switch to `Settings -> CI / CD -> Variables`

- REGISTRY_PASSWORD ( masked ) it must be encoded in base64 like this `echo -n "svc_password" | base64 -w0`, 
you can leave protected checked.

- REGISTRY_SERVER normally the value is static `registry.gitlab.com`, you can leave protected checked.

- REGISTRY_USERNAME not hidden, you can leave protected checked.

As your variables are protected, certain types of tag must be certified to be able to access your data, so I invite you to go to Settings -> Protected Tags then to create a wildcard with the pattern v*

## Prerequisites GCP

Installation instructions can be found [here](https://learn.hashicorp.com/terraform/getting-started/install.html).

This guide uses the Google Cloud Platform (GCP) utility `gcloud`, which is part of the [Cloud SDK](https://cloud.google.com/sdk/).
Installation instructions for this can be found [here](https://cloud.google.com/sdk/install).
The Google Cloud Storage (GCS) command line utility `gsutil` is also used to create and delete storage buckets.
This is installed as part of the Cloud SDK.
Much of what these commands do can be achieved using the GCP Console, but using the commands allows this guide to be concise and precise.

Once the Cloud SDK is installed you can authenticate, set the GCP project, and choose a compute zone with the interactive command:

```
gcloud init
```

Next ensure the required APIs are enabled in your GCP project:

```
gcloud services enable storage-api.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable container.googleapis.com
gcloud services enable iam.googleapis.com
```

Terraform needs some [credentials](https://www.terraform.io/docs/providers/google/guides/getting_started.html#adding-credentials)
to make requests against the GCP API.
Using a dedicated GCP service account is recommended as allows permissions to be limited to those required.
Create a service account for Terraform to use, generate a key file for it, and save the key location as an environment variable:

## Service Account

Terraform needs some [credentials](https://www.terraform.io/docs/providers/google/guides/getting_started.html#adding-credentials)
to make requests against the GCP API.
Using a dedicated GCP service account is recommended as allows permissions to be limited to those required.
Create a service account for Terraform to use, generate a key file for it, and save the key location as an environment variable:

```
PROJECT_ID=[ID OF YOUR PROJECT]
gcloud iam service-accounts create terraform
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member "serviceAccount:terraform@${PROJECT_ID}.iam.gserviceaccount.com" --role "roles/owner"
gcloud iam service-accounts keys create key.json --iam-account terraform@${PROJECT_ID}.iam.gserviceaccount.com

You can base64 encoded the json here `$PWD/key.json` as :

cat $PWD/key.json | base64 -w0
```

You can add a new variable hidden in gitlab with this name `GOOGLE_APPLICATION_CREDENTIALS` and with the content in base64

## Terraform vars

Once you have made the changes, it is necessary to take a look at the terraform part and adapt the variables to suit your environment.

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/terraform/k8s-dev/terraform.tfvars

Here are the madatory lines :

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/terraform/k8s-dev/terraform.tfvars#L15

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/terraform/k8s-dev/terraform.tfvars#L70 ( https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity )

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/terraform/k8s-dev/terraform.tfvars#L72


## FluxCD - GITOPS

The applications are deployed via the GITOPS methodology, this method is based on a source of truth (GIT), and comes PULL the manifests, it will be necessary to make some repository changes.

Here are the two micro-services with the lines concerned :

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/k8s-gitops/mlaude-medecine-devops-challenge/hr-publisher.yaml#L10

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/k8s-gitops/mlaude-medecine-devops-challenge/hr-publisher.yaml#L21

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/k8s-gitops/mlaude-medecine-devops-challenge/hr-worker.yaml#L10

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/k8s-gitops/mlaude-medecine-devops-challenge/hr-worker.yaml#L21

https://gitlab.com/binboum/medecine-devops-challenge-developer/-/blob/master/k8s-gitops/mlaude-medecine-devops-challenge/hr-redis.yaml#L414

## GIT

Once you have pre-configured your work environment, we will be able to move on to deploying the infrastructure.

We will start by running a pipeline that will build the images, and deploy the GKE cluster.

To do this we will tag in v0.1.0 for example :

```
git tag v0.1.0 && git push --tags
```

You can go to the Pipeline tabs, there will be a trigger to apply the configurations.

## Deployment

Once the cluster is deployed ~ 15m, in the output you will notice a public key, it is the CD tool named FluxCD which will need to download your fork repository to apply the configurations, so you need to add to your public key in this repository (read and write).

After cluster deployment, it takes around 25m of a minute for the tool to start up and deploy your environment.

Micro-services are in the namespace `mlaude-medecine-devops-challenge`, there is also a namespace `monitoring` which includes a monitoring server, and an adapter to be able to consume custom metrics.