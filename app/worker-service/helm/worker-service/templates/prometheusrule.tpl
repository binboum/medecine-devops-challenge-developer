{{- if .Values.autoscaling.enabled }}
---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: {{ include "worker-service.fullname" . }}
  labels:
    app: kube-prometheus-stack
    release: kube-prometheus-stack
spec: 
  groups:
  - name: messagequeue
    rules:
    - record: messages_waiting_in_queue_name
      expr: app_tab_count{namespace="{{ .Release.Namespace }}"}
      labels:
        namespace: {{ .Release.Namespace }}
        service: eventqueue
{{- end }}