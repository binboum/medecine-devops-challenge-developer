#!/usr/bin/env python
from jsonschema import validate, ValidationError, SchemaError
import time
import rediswq
import json
import uuid as uid
import os
import sys

# Set ENV
if "REDIS_SERVICE_HOST" in os.environ:
    redis_host = os.environ["REDIS_SERVICE_HOST"]
else:
    redis_host = "127.0.0.1"

if "REDIS_SERVICE_PORT" in os.environ:
    redis_port = os.environ["REDIS_SERVICE_PORT"]
else:
    redis_port = 6379


# Try to connect redis instance
try:
    q = rediswq.RedisWQ(
        name="tab",
        host=redis_host,
        port=redis_port,
    )
    sys.stdout.write("Connected!\n")
except Exception as ex:
    sys.stdout.write("Error: %s\n" % ex)
    sys.exit('Failed to connect, terminating.')

# q = rediswq.RedisWQ(name="tab", host="localhost")
print("Worker with sessionID: " +  q.sessionID())
print("Initial queue state: empty=" + str(q.empty()))

schema = {
    "type" : "object",
    "properties" : {
        "uuid" : {"type" : "string"},
        "nbr_tab" : {
            "type" : "number",
        }
    },
    "required": ["uuid","nbr_tab"]
}

if __name__ == "__main__":
    while(1):
        for i in range(10):
            try:
                item = q.lease(lease_secs=10, block=True, timeout=2)
            except:
                sys.exit('Failed to connect, retry.')
                time.sleep(5)
                continue
            else:
                break
        else:
            sys.exit('Failed to connect, terminating.')

        if item is not None:
            itemstr = item.decode("utf-8")

            # check if json is correct
            try:
                itemlist = json.loads(itemstr)
            except ValueError as e:
                sys.stdout.write("Not valid json: %s\n" % itemstr)
                q.complete(item)
                continue

            # check if json schema is correct
            try:
                validate(itemlist, schema)
            except SchemaError as e:
                sys.stdout.write("There is an error with the schema: %s\n" % e)
                q.complete(item)
                continue
            except ValidationError as e:
                sys.stdout.write("ValidationError %s\n" % e)
                q.complete(item)
                continue

            sys.stdout.write("\nPatient : %s\n" % itemlist["uuid"])

            # for each tab with sleep 2s
            for i in range(0, itemlist["nbr_tab"]):
                uuid = uid.uuid1()
                time.sleep(2)
                sys.stdout.write("Tab-n : %s UUID : %s \n" % (i+1, uuid.hex))

            q.complete(item)
        else:
            sys.stdout.write("Waiting for work\n")