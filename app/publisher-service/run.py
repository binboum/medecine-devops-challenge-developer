#!/usr/bin/env python
from random import randint
import time
import os
import sys
import redis
import json
import uuid

# Set ENV
if "REDIS_SERVICE_HOST" in os.environ:
    redis_host = os.environ["REDIS_SERVICE_HOST"]
else:
    redis_host = "127.0.0.1"

if "REDIS_SERVICE_PORT" in os.environ:
    redis_port = os.environ["REDIS_SERVICE_PORT"]
else:
    redis_port = 6379

# Try to connect redis instance
try:
    redis_conn = redis.Redis(
        host=redis_host,
        port=redis_port,
        charset="utf-8", 
        decode_responses=True
    )
    redis_conn.ping()
    sys.stdout.write("Connected!\n")
except Exception as ex:
    sys.stdout.write("Error: %s\n" % ex)
    sys.exit('Failed to connect, terminating.')


# def pub function for send a data to redis
def pub(number):
    data = {
        "uuid": str(uuid.uuid1()),
        "nbr_tab": number,
    }

    for i in range(10):
        try:
            redis_conn.rpush('tab', json.dumps(data))
        except:
            sys.exit('Failed to connect, retry.')
            time.sleep(5)
            continue
        else:
            break
    else:
        sys.exit('Failed to connect, terminating.')


if __name__ == "__main__":
    while (1):
        time.sleep(1)  # each second for generate seed
        tab = randint(1, 30)
        sys.stdout.write("ask: %s\n" % tab)
        pub(tab)