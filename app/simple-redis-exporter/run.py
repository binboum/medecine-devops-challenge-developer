#!/usr/bin/env python
# https://trstringer.com/quick-and-easy-prometheus-exporter/
import os
import sys
import re
import time
from prometheus_client import start_http_server, Gauge, Enum
import redis


full_host = re.search(r"redis:\/\/(.*):(.*)", os.environ["REDIS_ADDR"], re.IGNORECASE)

redis_host = full_host.group(1)
redis_port = full_host.group(2)

# Try to connect redis instance
try:
    redis_conn = redis.Redis(
        host=redis_host,
        port=redis_port,
        charset="utf-8", 
        decode_responses=True
    )
    redis_conn.ping()
    sys.stdout.write("Connected!\n")
except Exception as ex:
    sys.stdout.write("Error: %s\n" % ex)
    sys.exit('Failed to connect, terminating.')

class AppMetrics:
    """
    Representation of Prometheus metrics and loop to fetch and transform
    application metrics into Prometheus metrics.
    """

    def __init__(self, app_port=6379, polling_interval_seconds=5):
        self.app_port = app_port
        self.polling_interval_seconds = polling_interval_seconds

        # Prometheus metrics to collect
        self.current = Gauge("app_tab_count", "tab count")

    def run_metrics_loop(self):
        """Metrics fetching loop"""

        while True:
            self.fetch()
            time.sleep(self.polling_interval_seconds)

    def fetch(self):
        """
        Get metrics from application and refresh Prometheus metrics with
        new values.
        """
        # Update Prometheus metrics with application metrics
        try:
            getCount = redis_conn.rpush( 'tab', "tab" )
            self.current.set(getCount)
        except:
            self.current.set(0)

def main():
    """Main entry point"""

    polling_interval_seconds = int(os.getenv("POLLING_INTERVAL_SECONDS", "5"))
    app_port = int(os.getenv("APP_PORT", "6379"))
    exporter_port = int(os.getenv("EXPORTER_PORT", "9121"))

    app_metrics = AppMetrics(
        app_port=app_port,
        polling_interval_seconds=polling_interval_seconds
    )
    start_http_server(exporter_port)
    app_metrics.run_metrics_loop()

if __name__ == "__main__":
    main()