output "flux_public_key" {
  value = tls_private_key.flux_repo.*.public_key_openssh
}