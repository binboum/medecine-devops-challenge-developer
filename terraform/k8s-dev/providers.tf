terraform {
  required_version = "~> 0.12"
  backend "http" { }
}

provider "kubernetes" {
    host    = "https://${data.google_container_cluster.cluster.endpoint}"
    token   = data.google_client_config.current.access_token
    cluster_ca_certificate = base64decode(data.google_container_cluster.cluster.master_auth.0.cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host    = "https://${data.google_container_cluster.cluster.endpoint}"
    token   = data.google_client_config.current.access_token
    cluster_ca_certificate = base64decode(data.google_container_cluster.cluster.master_auth.0.cluster_ca_certificate)
  }
}

provider "google" {
  version = "3.5.0"
  project = var.gcp_project_id
  region  = local.gcp_region
}