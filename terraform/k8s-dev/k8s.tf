data "google_client_config" "current" {}

data "google_container_cluster" "cluster" {
  depends_on = [module.cluster]
  name     = var.cluster_name
  location = var.gcp_location
}

resource "kubernetes_namespace" "loft-system" {
  depends_on = [module.cluster]
  metadata {
    annotations = {
      name = "loft-system"
    }

    name = "loft-system"
  }
}

resource "tls_private_key" "flux_repo" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "kubernetes_secret" "git-flux" {
  depends_on = [kubernetes_namespace.loft-system]
  metadata {
    name      = "flux-git-deploy"
    namespace = "loft-system"
  }
  data = {
    identity = tls_private_key.flux_repo.private_key_pem
  }
  type = "Opaque"
}

resource "helm_release" "flux" {
  depends_on = [kubernetes_secret.git-flux]
  name       = "flux"
  namespace  = "loft-system"
  repository = "https://charts.fluxcd.io"
  chart      = "flux"
  version    = "1.6.2"

    set {
        name  = "git.url"
        value = var.repo_ssh
    }

    set {
        name  = "git.branch"
        value = "master"
    } 

    set {
        name  = "git.branch"
        value = "master"
    }

    set {
        name  = "git.secretName"
        value = "flux-git-deploy"
    }


    set {
        name  = "git.path"
        value = "k8s-gitops"
    }
}

resource "helm_release" "helm-operator" {
  depends_on = [kubernetes_secret.git-flux]
  name       = "helm-operator"
  namespace  = "loft-system"
  repository = "https://charts.fluxcd.io"
  chart      = "helm-operator"
  version    = "1.2.0"

    set {
        name  = "helm.versions"
        value = "v3"
    }

    set {
        name  = "git.ssh.secretName"
        value = "flux-git-deploy"
    }
}

resource "kubernetes_namespace" "mlaude-medecine-devops-challenge" {
  depends_on = [module.cluster]
  count = var.create_challenge_ns ? 1 : 0
  metadata {
    annotations = {
      name = "mlaude-medecine-devops-challenge"
    }

    name = "mlaude-medecine-devops-challenge"
  }
}

resource "kubernetes_secret" "regcred" {
  depends_on = [kubernetes_namespace.mlaude-medecine-devops-challenge]
  metadata {
    namespace  = "mlaude-medecine-devops-challenge"
    name = "regcred"
  }

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "${var.registry_server}": {
      "auth": "${base64encode("${var.registry_username}:${var.registry_password}")}"
    }
  }
}
DOCKER
  }

  type = "kubernetes.io/dockerconfigjson"
}