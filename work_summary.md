## Work summary

I started by assimilating the subject in order to fully understand the way the producer and consumer services would interact with each other because of treatment delay.

I took a moment to think about the architecture and its design. Particularly about the tools that I was going to use based on the deadlines and also on my knowledge.

To optimize time, I tried to rely as much as possible on the components of gitlab.

On the development part and the message distribution part, my choice was redis as it is relatively efficient. If we add a load balancer layer like haproxy we can quickly work on the added value and save time on the development part.

After some research I looked at the RPUSH method of redis in order to gain reliability and manage a buffer memory.

The subject is very comprehensive, overall we touch on a complete automated management ecosystem, there are a lot of bricks that come together, and I had to make choices on the priorities.

Some parts need to be improved, such as network policies between micro services.
For example haproxy needs to directly reach redis, but micro-services can only communicate with haproxy.

For the password management part, it is difficult to set up a complete solution to manage passwords because the public helm charts are not a suitable environment to configure a vault.
Placing passwords in clear text on GIT is not recommended.

There is a different way of approaching the question, we often find Bitnami's Sealed Secret which allows encryption to then push on GIT which is then live decrypted on the cluster. Or Vault which allows through modules to inject the passwords directly into the containers but this requires rewriting.

For the monitoring part, I wanted to set up a custom request that would be usable by HPA Kubernetes to horizontally scale workers according to traffic. I had never done it this way and it required a little time to adapt, especially because it was necessary to set up a prometheus, and prometheus adapter to manage customizable metrics.

In order to manage the resource allocation, I have defined requests and limits. Because of the low latency rate offered to workers, they may take a little time to launch, but loop reasonably afterwards. (it is possible to tune, I wanted to carry out tests on several scales in order to see how the application responded).

Redis remaining a sensitive brick it is deployed in a High Availability setup.
